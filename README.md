# Pokemon Pipeline

Have a look at [Pokemon Evolution in GitHub](https://github.com/Lorellana21/pokemon-evolution)

This is an evolution of such an exercise 🚀 

![image](https://user-images.githubusercontent.com/81922944/155576311-b2d96d57-6c08-40ab-94ff-7658a9d5e456.png)


Here the objective is to create a **pipeline** in a **React project**.

Review this file: **.gitlab-ci.yml**, and see the steps I followed to make it.
